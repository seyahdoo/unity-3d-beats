using System;
using System.IO;
using UnityEngine;
using Elastic.Clients.Elasticsearch;
using Elastic.Transport;
using UnityEditor;

namespace Elastic.Beats {
    public class Beats
    {
        //batch data for 500 or sth frames.
        //work on a seperate thread

        private static ElasticsearchClient client;
    
        public static void Initialize() {
            var beatsSettings = GetSettings();
            var settings = new ElasticsearchClientSettings(new Uri(beatsSettings.uri))
                .Authentication(new Base64ApiKey(beatsSettings.apiKey));
            client = new ElasticsearchClient(settings);
        }

        private static ElasticBeatsSettings GetSettings() {
            var beatsSettings = Resources.Load<ElasticBeatsSettings>(nameof(ElasticBeatsSettings));
            if (beatsSettings != null) {
                return beatsSettings;
            }
            #if UNITY_EDITOR
                beatsSettings = ScriptableObject.CreateInstance<ElasticBeatsSettings>();
                Directory.CreateDirectory("Assets/Resources");
                var path = $"Assets/Resources/{nameof(ElasticBeatsSettings)}.asset";
                AssetDatabase.CreateAsset(beatsSettings, path);
                Debug.Log($"Could not find {nameof(ElasticBeatsSettings)}, creating a new one at {path}");
                return beatsSettings;
            #else
                throw new Exception($"Could not found {nameof(ElasticBeatsSettings)}");
            #endif
        }

        public static void SendCustomEvent(object customData) {
        
        }

        public static void StartCapturingFrames() {
        
        }

        public static void StopCapturingFrames() {
            Debug.Log("asd");
        }
    }
}


