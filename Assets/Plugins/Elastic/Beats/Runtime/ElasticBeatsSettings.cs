using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Elastic.Beats {
    public class ElasticBeatsSettings : ScriptableObject {
        public bool autoCaptureFrames;
        public string uri;
        public string apiKey;
    }
}
